<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCommentsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('comments', function (Blueprint $table) {
				$table->increments('id');
				$table->text('text');

				// added the relation or FK for the post
				$table->integer('user_id');
				// path of its table
				$table->foreign('user_id')->references('id')->on('users');

				// added the relation or FK for the post
				$table->integer('post_id');
				// path of its table
				$table->foreign('post_id')->references('id')->on('posts');
				$table->timestamps();
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('comments');
	}
}
