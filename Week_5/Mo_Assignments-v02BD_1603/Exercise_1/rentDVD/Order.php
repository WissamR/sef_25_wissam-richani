<?php

session_start();

require_once "libs/SakilaWrapper.php";
require_once "libs/Config.php";

// New DB Object
$mysqlObj = new SakilaWrapper();
// Errors Array
$generalError = array();

/**
 * Query the DB for customer information
 */
// Get Customer Information
$customersList = $mysqlObj->selectCustomers();
// Init CName
$customerName = 'Undefined';
// Check if the customer was not returned
if (!empty($customersList) &&
	!$customersList[0]->isEmpty()) {
	// Get the customer name
	$customerName = $customersList[0]->customer_name;
} else {
	// Push the customer not retrieve error
	array_push($generalError, "Customer was not retrieved!");
}

$searchNeedle = '';
$movieCount   = 0;
$currentPage  = 1;
/**
 * Handle the user input for Movie Searches
 */
$movieList = array();
// Get the list of movies based on the search query
if (isset($_POST['movieNameSearch'])) {
	// Movie Search needle
	$searchNeedle = $_POST['movieNameSearch'];
	// Store the needle in a session variable
	$_SESSION['searchNeedle'] = $searchNeedle;
	// query the DB
	$movieList = $mysqlObj->searchMovieTitle($searchNeedle);
	// Total Movies Count
	$movieCount = $mysqlObj->getMovieCount($searchNeedle);
	// Set the session
	$_SESSION['movieCount'] = $movieCount;
	// Check if the query returned any results
	if (empty($movieList)) {
		array_push($generalError, "Your search '{$searchNeedle}' returned 0 results.");
	}
} elseif (isset($_SESSION['searchNeedle']) &&
	isset($_SESSION['movieCount'])) {
	// If the session value is set we use it to query
	// information
	$searchNeedle = $_SESSION['searchNeedle'];
	$p
	// query the DB
	$movieList = $mysqlObj->searchMovieTitle($searchNeedle, (($page-1)*RESULTS_PPAGE), RESULTS_PPAGE);
	// Total Movies Count
	$movieCount = $_SESSION['movieCount'];
	// Check if the query returned any results
	if (empty($movieList)) {
		array_push($generalError, "Your search '{$searchNeedle}' returned 0 results.");
	}
}

/**
 * Deal with the pagination
 */
// Get the total number of pages needed
$numberOfPages = ceil($movieCount/RESULTS_PPAGE);

if (isset($_GET['page'])) {
	$currentPage = $_GET['page'];
	// Validate that no tampering has happened
	if (!is_numeric($currentPage) ||
		empty($currentPage)) {
		array_push($generalError, "Tampering with the page value is not allowed!");
	} else if ($currentPage < 0 ||
		$currentPage > $numberOfPages) {
		array_push($generalError, "Tampering with the page value is not allowed!");
	}
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>yourSakilaShop</title>
	<link rel="stylesheet" type="text/css" href="./css/styles.css">
</head>

<body>
	<header id="header">
		<h1>Welcome to <span class="red"><?php echo $mysqlObj->getDBName();?></span></h1>
		<hr />
		<h2>Welcome <span class="underline"><?php echo "{$customerName}";?></span></h2>
	</header>

<?php
/**
 * Check the Database Connection
 */
$dbConStatus = $mysqlObj->getDBStatus();
// Add the DB error in case it exists to the
// errors array
if ($dbConStatus) {
	array_push($generalError, $dbConStatus);
}
// Set the error-box visibility value
$errorVisibility = (!empty($generalError))?'shown':'hidden';
?>
	<div id="error-box" class="<?php echo $errorVisibility;?>">
<?php
/**
 * Display the error messages
 */
foreach ($generalError as $errorMsg) {
	echo "- {$errorMsg} <br />";
}
?>
	</div>

	<div id="search">
		<form id="searchForm" name="searchForm" action="" method="POST">
			<input type="text" name="movieNameSearch" placeholder="Movie Title" value="<?php echo $searchNeedle;?>"/>
			<button type="submit" name="search" class="submitBtn">Search</button>
		</form>
	</div>

	<div id="searchResults">
		<form id="resultsForm" name="resultsForm" action="OrderProcess.php" method="POST">
		<!-- Search Results Table -->
			<table id="resultsTable">
				<tbody>

<?php

/**
 * Display the list of returned movies
 */
foreach ($movieList as $movie) {

	?>


								<!-- Movie Block -->
								<tr class="td-emphasis less-padding">
									<td class="td-center"><?php echo $movie->Id;?></td>
									<td><?php echo "{$movie->Title} - {$movie->Rating}";?></td>
									<td>Stock Availability</td>
									<td>Rent Rate</td>
								</tr>

								<tr>
									<td class="td-center"><input type="checkbox" value="id" /></td>
									<td><?php echo $movie->Description;?></td>
									<td class="td-center"><?php echo $movie->inventoryCount;?></td>
									<td class="td-center"><?php echo "{$movie->rentalRate} USD";?></td>
								</tr>

								<tr class="borderless">
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								<!-- Movie Block END -->

	<?php
}
?>

				</tbody>
				<tfoot>
					<tr class="borderless">
						<td></td>
						<td></td>
						<td></td>
					</tr>

					<tr class="borderless <?php echo ($numberOfPages)?'':'hidden';?>">
						<td></td>
						<td class="pagination-container">
<?php
// Get the URI
$fullURL = "{$_SERVER['SCRIPT_NAME']}";
// Generate the visible set
$pageSet = range($currentPage-2, $currentPage+2);
// Filter the unwanted numbers
$result = array_filter($pageSet, function ($var) use ($numberOfPages) {
		return ($var >= 1 && $var <= $numberOfPages);
	});
?>
							<a href="<?php echo "{$fullURL}?page=1";?>">&lt;
&lt;
</a>
							<a href="javascript:;">&lt;
</a>
<?php
// Add the first page if need be
if ($currentPage >= 4) {
	echo "<a href=\"{$fullURL}?page=1\">1</a>\n";
	echo "<span>...</span>\n";
}
// Display the values in between
foreach ($result as $r) {
	if ($r == $currentPage) {
		echo "<a href=\"{$fullURL}?page={$r}\" class=\"current-page\">{$r}</a>\n";
	} else {
		echo "<a href=\"{$fullURL}?page={$r}\">{$r}</a>\n";
	}
}
// Add the final page if need be
if ($currentPage <= $numberOfPages-3) {
	echo "<span>...</span>\n";
	echo "<a href=\"{$fullURL}?page={$numberOfPages}\">{$numberOfPages}</a>\n";
}
?>
							<a href="javascript:;">&gt;
</a>
							<a href="<?php echo "{$fullURL}?page={$numberOfPages}";?>">&gt;
&gt;
</a>
						</td>
						<td></td>
					</tr>
				</tfoot>
			</table>
		<!-- Search Results Table END -->
		<button type="submit" class="submitBtn top-margin-50">Rent Movies</button>
		</form>
	</div>
</body>
</html>