<?php

namespace MyBlog;

use Illuminate\Database\Eloquent\Model;

class Post extends Model {
	protected $table = 'posts';
}
