<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('items', function (Blueprint $table) {
				$table->engine = 'InnoDB';

				$table->increments('product_id');
				$table->string('name', 45);
				$table->text('describtion');
				$table->decimal('price', 10, 0);
				$table->string('photo', 64);

				$table->integer('brand_id')->unsigned();
				$table->foreign('brand_id')
				->references('brand_id')->on('brands')
				->onDelete('CASCADE')
					->onUpdate('CASCADE');

				$table->integer('category_id')->unsigned();
				$table->foreign('category_id')
				->references('category_id')->on('categories')
				->onDelete('CASCADE')
					->onUpdate('CASCADE');

				$table->integer('item_type_id')->unsigned();
				$table->foreign('item_type_id')
				->references('item_type_id')->on('items_types')
				->onDelete('CASCADE')
					->onUpdate('CASCADE');

				$table->integer('item_offer_id')->unsigned();
				$table->foreign('item_offer_id')
				->references('item_offer_id')->on('items_offers')
				->onDelete('CASCADE')
					->onUpdate('CASCADE');

				$table->timestamp('current_date')->nullable()->default(NULL);
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('items');
	}
}
