<?php
require_once 'Database.php';
class Table {
	private $name;
	private $pathOfTable;
	private $tHeader;
	private $record;


	function __construct($newTable,$newPathOfTable,$tHeader) {
		$this->pathOfTable = $newPathOfTable;
		$this->name = $newTable;
		$this->tHeader = $tHeader;
		$this->createTable();
	}

	public function createTable() {
		if (!file_exists($this->pathOfTable."/".$this->name)) {
			$myFile = fopen("{$this->pathOfTable}/{$this->name}.csv", "w");
			fwrite($myFile,explode(",", $this->tHeader));
			fclose($myFile);
			echo "'$this->name' CREATED\n";
		} else {
			echo "Error, '$this->name' already EXISTS!\n";
		}
	} 
}
$db = new Database("ryan7");

$tableTest = new Table ("student",$db->getPath(), $tHeader);

?>