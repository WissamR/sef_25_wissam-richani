@extends('layouts.app')

@section('content')
<section id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div id="details-form" class="col-md-12"><!--login form-->
						<h2>Product Details</h2>
						<form action="#">
							<input type="text" placeholder="Product Name" />
							<textarea placeholder="Product Description"></textarea>
							<input type="text" placeholder="Product ID" />
							<input type="text" placeholder="Product Price" />
						</form>
					</div><!--/login form-->
				</div>
				<div class="col-sm-3">
					<div id="category-form" class="col-md-12"><!--sign up form-->
						<h2>Category</h2>
						<form action="#">
							<input type="text" placeholder="Category Name"/><br>
							<input type="text" placeholder="Gender"/><br>
							<input type="checkbox" class="New Product" value="New Product" /> New Product<br>
							<input type="text" placeholder="Name"/><br>
						</form>
					</div><!--/sign up form-->
				</div>
				<div class="col-sm-3">
					<div id="browse-form" class="col-md-12">
						<h2>Browse image</h2>
						 <form action="#">
						  <div><input style="border:1px solid #aaa; border-radius:4px;" type="file" name="pic" accept="image/*"></div>
						  <input type="submit" value="Save Updates">
						</form>
					</div>
				</div>
			</div>
		</div>
	</section><!--/form-->
@endsection
<!-- <button type="submit" class="btn btn-default">Signup</button> -->
<!-- <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }} -->