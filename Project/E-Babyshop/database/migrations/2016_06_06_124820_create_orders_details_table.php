<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersDetailsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('orders_details', function (Blueprint $table) {
				$table->engine = 'InnoDB';

				$table->increments('order_detail_id');

				$table->integer('order_id')->unsigned();
				$table->foreign('order_id')
				->references('order_id')->on('orders')
				->onDelete('CASCADE')
					->onUpdate('CASCADE');

				$table->integer('product_id')->unsigned();
				$table->foreign('product_id')
				->references('product_id')->on('items')
				->onDelete('CASCADE')
					->onUpdate('CASCADE');

				// $table->timestamps();
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('orders_details');
	}
}
