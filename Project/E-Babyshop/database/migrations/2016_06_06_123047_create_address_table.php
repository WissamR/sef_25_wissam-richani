<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAddressTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('address', function (Blueprint $table) {
				$table->engine = 'InnoDB';

				$table->increments('address_id')->unsigned();
				$table->string('name', 200);
				$table->string('phone', 20);

				$table->integer('country_id')->unsigned();
				$table->foreign('country_id')
				->references('country_id')->on('countries')
				->onDelete('CASCADE')
					->onUpdate('CASCADE');
				// $table->timestamps();
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('address');
	}
}
