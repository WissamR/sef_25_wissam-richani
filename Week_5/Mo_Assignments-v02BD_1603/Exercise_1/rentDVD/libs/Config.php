<?php
/**
 * General DB access credentials
 */
define('DB_HOST', 'localhost');
define('DB_USER', 'phpwissam');
define('DB_PASSWORD', 'compl3xPassw0rd');
define('DB_SCHEMA', 'sakila');
define('RESULTS_PPAGE', 10);