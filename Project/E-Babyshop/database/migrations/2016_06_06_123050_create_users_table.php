<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('users', function (Blueprint $table) {
				$table->increments('user_id');
				$table->string('name');
				$table->string('firstname', 128);
				$table->string('lastname', 128);
				$table->string('email')->unique();
				$table->tinyInteger('admin')->default('0');
				$table->string('phone', 25);
				$table->string('password');
				$table->rememberToken();
				$table->string('country');
				$table->string('location');
				$table->string('address');
				$table->timestamps();

				$table->integer('address_id')->unsigned()->nullable()->unique();
				$table->foreign('address_id')
				->references('address_id')->on('address')
				->onDelete('CASCADE')
					->onUpdate('CASCADE');
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('users');
	}
}
