<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemsOffersTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('items_offers', function (Blueprint $table) {
				$table->engine = 'InnoDB';

				$table->increments('item_offer_id');
				$table->string('name', 128)->nullable()->default(NULL);

				// $table->timestamps();
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('items_offers');
	}
}
