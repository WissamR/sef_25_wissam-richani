<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemsTypesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('items_types', function (Blueprint $table) {
				$table->engine = 'InnoDB';

				$table->increments('item_type_id');
				$table->string('name', 45)->nullable()->default(NULL);

				// $table->timestamps();
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('items_types');
	}
}
