<?php
class Database {

	private $path = "/home/wissam-ubuntu/Desktop/Database/";

	function __construct($dbName) {
		$this->path .= $dbName;
		$this->create($dbName);
	}

	public function create($database) {
		if (!file_exists($this->path)) {
			mkdir($this->path);
			echo "'$database' CREATED\n";
		} else {
			echo "Error, '$database' already EXISTS!\n";
		}
	}

	static function delete($PathOfDb) {
		if (is_dir($PathOfDb)) {
			$files = glob($PathOfDb.'*', GLOB_MARK);//glob adds a slash to each directory returned.
			//The * matches 0 or more of any character except a /
			foreach ($files as $file) {
				self::delete("$PathOfDb/$file");
			}
			rmdir($PathOfDb);
		} elseif (is_file($PathOfDb)) {
			unlink($PathOfDb);
		}
	}

	public function getPath() {
		return $this->path;
	}

}

?>