<?php

namespace MyBlog\Http\Controllers;

use MyBlog\Post;

class PostController extends Controller {
	// list all the posts
	public function sendView() {
		// get the data
		$allPosts = Post::all();
		//send the view
		return view('listpost', compact('allPosts'));
	}

	// add new post
	public function addPost() {
		$myNewPost = new Post();

		$myNewPost->title       = "Hello test 1";
		$myNewPost->description = "test test";

		// save new post to db
		if ($myNewPost::save()) {
			return "Post added Successfully";
		} else {
			return "Error, post was not Added!"
		}
	}

}
