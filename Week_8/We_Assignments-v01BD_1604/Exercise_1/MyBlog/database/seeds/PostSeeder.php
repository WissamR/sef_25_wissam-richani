<?php

use Illuminate\Database\Seeder;

class PostSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::table('posts')->insert([
				['title'       => 'Hello blog 1',
					'description' => 'Today seems like a very beautiful day!'],

				['title'       => 'Get Out of My Damn Chair.',
					'description' => 'There have been a wave of think pieces about Black women and electoral politics (many of which are brilliant, and that I read obsessively, like here & here & here) where compelling data and narratives on our nuanced place in our political history are argued, but I have to be honest with you: this is not one of them.,'],

				['title'       => 'Ten Years of Giving, Together.',
					'description' => '‘One for One’ has already left a bigger footprint than a pair of shoes… and this is just the beginning.'],
			]);
	}
}
