<?php

namespace App\Http\Controllers;

class AddController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		// $this->middleware('auth');// if you want users to seem the home page without login take it out.
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function add() {
		return view('add');
	}
}