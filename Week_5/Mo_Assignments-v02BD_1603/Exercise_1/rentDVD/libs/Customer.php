<?php

class Customer {
	/**
	 * [$customer_id description]
	 *
	 * @var [type]
	 */
	public $customer_id;
	/**
	 * [$customer_name description]
	 *
	 * @var [type]
	 */
	public $customer_name;
	/**
	 * [$active description]
	 *
	 * @var [type]
	 */
	public $active;

	/**
	 * [__construct description]
	 *
	 * @param [type] $id     [description]
	 * @param [type] $name   [description]
	 * @param [type] $active [description]
	 */
	public function __construct($id, $name, $active) {
		$this->customer_id = $id;
		$this->customer_name = $name;
		$this->active = $active;
	}

	public function getFirstName() {
		$arr = explode(' ', $this->customer_name);
		return $arr[0];
	}

	/**
	 * [isEmpty description]
	 *
	 * @return boolean [description]
	 */
	public function isEmpty() {
		// Returns True if customer_id is not empty
		// Returns False otherwise
		return empty($this->customer_id);
	}
}