<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('orders', function (Blueprint $table) {
				$table->engine = 'InnoDB';

				$table->increments('order_id');
				$table->float('amount')->nullable()->default(NULL);

				$table->integer('user_id')->unsigned();
				$table->foreign('user_id')
				->references('user_id')->on('users')
				->onDelete('CASCADE')
					->onUpdate('CASCADE');

				$table->integer('address_id')->unsigned();
				$table->foreign('address_id')
				->references('address_id')->on('address')
				->onDelete('CASCADE')
					->onUpdate('CASCADE');

				// $table->timestamps();
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('orders');
	}
}
